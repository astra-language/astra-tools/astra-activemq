package jms;

import java.util.HashMap;
import java.util.Map;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;

import org.apache.activemq.ActiveMQConnectionFactory;

import astra.core.Module;
import astra.event.Event;
import astra.reasoner.Unifier;
import astra.term.Primitive;
import astra.term.Term;

public class ActiveMQ extends Module {
	static {
		Unifier.eventFactory.put(JMSEvent.class, new JMSEventUnifier());
	}

    public static final Primitive<String> TOPIC_TYPE = Primitive.newPrimitive("TOPIC");
    public static final Primitive<String> QUEUE_TYPE = Primitive.newPrimitive("QUEUE");
    private Session session;

    @ACTION
    public boolean initialize(String host) {
        return initialize(host, agent.name());
    }

    @ACTION
    public boolean initialize(String host, String clientName) {
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("failover://tcp://"+host+":61616");
        try {
            Connection connection = connectionFactory.createConnection();
            connection.setClientID(clientName);
            session = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);
            connection.start();
        } catch (JMSException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @ACTION
    public boolean subscribe(String topicName) {
        try {
            Topic topic = session.createTopic(topicName);
            MessageConsumer consumer = session.createConsumer(topic);
            consumer.setMessageListener(new MessageListener() {
                public void onMessage(Message message) {
                    try {
                        String json = ((TextMessage) message).getText();
                        agent.addEvent(new JMSEvent(TOPIC_TYPE, Primitive.newPrimitive(topicName), Primitive.newPrimitive(json)));
                        message.acknowledge();
                    } catch (JMSException e) {
                        System.out.println("[ActiveMQ.WARNING] Unhandled message:\n" + message.toString());
                    }
                }
            });

        } catch (JMSException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @ACTION
    public boolean dequeue(String name) {
        try {
            Queue queue = session.createQueue(name);
            MessageConsumer consumer = session.createConsumer(queue);
            consumer.setMessageListener(new MessageListener() {
                public void onMessage(Message message) {
                    try {
                        String json = ((TextMessage) message).getText();
                        agent.addEvent(new JMSEvent(QUEUE_TYPE, Primitive.newPrimitive(name), Primitive.newPrimitive(json)));
                        message.acknowledge();
                    } catch (JMSException e) {
                        System.out.println("[ActiveMQ.WARNING] Unhandled message:\n" + message.toString());
                    }
                }
            });

        } catch (JMSException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private Map<String,MessageProducer> producers = new HashMap<>();

    @ACTION
    public boolean publish(String topicName, String text) {
        try {
            MessageProducer producer = producers.get(topicName);
            if (producer == null) {
                Topic topic = session.createTopic(topicName);
                producer  = session.createProducer(topic);
                producers.put(topicName, producer);
            }
                
            TextMessage textMessage = session.createTextMessage(text);
            producer.send(textMessage);
        } catch (JMSException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    private Map<String,MessageProducer> queueProducers = new HashMap<>();

    @ACTION
    public boolean enqueue(String name, String text) {
        try {
            MessageProducer producer = queueProducers.get(name);
            if (producer == null) {
                Queue queue = session.createQueue(name);
                producer  = session.createProducer(queue);
                queueProducers.put(name, producer);
            }
                
            TextMessage textMessage = session.createTextMessage(text);
            producer.send(textMessage);
        } catch (JMSException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

	@EVENT( types = { "string", "string", "string" }, signature="$jms:", symbols = {} )
	public Event event(Term type, Term source, Term content) {
		return new JMSEvent(type, source, content);
	}

}